<?php
namespace Deployer;

require 'recipe/symfony3.php';

set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('writeable_mode', 'chmod');
set('repository', 'git@bitbucket.org:peterhoefnagel/skrepr-talk-peter.git');

host('stalk')
    ->hostname('188.226.154.235')
    ->user('deploy')
    ->set('branch', 'master')
    ->set('deploy_path', '/home/deploy/skrepr-talk.nl');