server {
    listen 80;
    server_name 188.226.154.235;
    root /home/deploy/skrepr-talk.nl/current/web;
    index app.php;
    error_log /var/log/skrepr-talk.log;

    location ~ "^(.+\.php)($|/)" {
        fastcgi_split_path_info ^(.+\.php)(.*)$;

        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_pass  unix:/run/php/php7.1-fpm.sock;
        include       fastcgi_params;
    }
}