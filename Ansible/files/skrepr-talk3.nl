server {
    listen 80;
    server_name skrepr-talk3.nl;
    root /home/deploy/skrepr-talk3.nl/current/web;
    index app.php;

    location ~ "^(.+\.php)($|/)" {
        fastcgi_split_path_info ^(.+\.php)(.*)$;

        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_pass  unix:/run/php/php7.1-fpm.sock;
        include       fastcgi_params;
    }
}